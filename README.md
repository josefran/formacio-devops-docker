# Formació DevOps - Docker

## Requisits

Requereix tenir instal·lat previament:
1. Git https://git-scm.com/
2. Virtualbox https://www.virtualbox.org/
3. Vagrant https://www.vagrantup.com/

## Pasos a seguir

Clonar el repositori GIT:

```bash
git clone https://gitlab.com/josefran/formacio-devops-docker.git
```

Accedir al directori `formacio-devops-docker`:

```bash
cd formacio-devops-docker
```

Iniciar la màquina virtual amb les comandes de `Vagrant`:

```bash
vagrant up
```

Accedir a la màquina virtual per SSH, per poder realitzar el curs:

```bash
vagrant ssh
```

Comprovar que funciona `docker` amb la següent comanda:

```bash
docker --version
docker ps
docker-compose --version
```

## Possibles errors

### Problemes amb `vagrant ssh`
Si no aconseguim accedir amb `vagrant ssh`, realitzem les següents passes:

1. Accedir a Virtualbox, seleccionar la màquina virtual de la formació devops docker i premer el botó de "Mostrar". 
2. Accedir a la pantalla de terminal de la màquina virtual de Virtuabbox. Això desbloquejarà la creació de la màquina virtual.
3. Tornar la línia de comandes, i executar:

```bash
vagrant provision
vagrant ssh
```

Tanbé podeu accedir directament a la màquina virtual, via Virtualbox, amb usuari `vagrant` i contrasenya `vagrant`.

### Problemes amb `docker`

Si tenim accés SSH, però no tenim `docker` instal·lat, és a dir, no funciona la comanda `docker --version`, executem les següents comandes fora de la màquina virtual:

```bash
vagrant provision
vagrant ssh
```

### Problemes amb 'docker-compose`

Si tenim accés SSH, però falla la comanda `docker-compose --version`, podeu instal·lar `docker-compose` manualment:

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

A vegades es descarrega malament, repetiu la comanda fins que funcioni.
